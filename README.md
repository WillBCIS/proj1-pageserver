# README #
Simple web server to test understanding of web architecture
Author(s): Ram Drairajan; William Bucher

Contact: wbucher@uoregon.edu

Description:
This program demonstrates a simple web server. 
Most of the code was written by Professor Ram Durairajan, however, my goal is 
to add additional responses from the client that either display a web page,
or throw exceptions(404/403) depending on which violations occured.